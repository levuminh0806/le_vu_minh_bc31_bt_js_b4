// input : user nhập 3 cạnh a , b ,c ( tương ứng canhAValue,canhBValue,canhCValue trong bài)

// Các bước xử lí : 
// 1 . tam giác đều ( đk : 3 cạnh a = b = c )
// trường hợp 1 : đúng => tam giác đều
// trường hợp 2 : sai  => xét tiếp
// 2.  tam giác cân ( đk : 2 cạnh bất kì bằng nhau)
// trường hợp 3 : đúng => tam giác cân
// trường hợp 4 : sai => xét tiếp
// 3. tam giác vuông ( đk : c > a && c > b (để đảm bảo cạnh c là canh huyênf))
// trường hợp 5 : => đúng
// trường hợp 6 : => xét tiếp
// 4. tam giác khác

// trong đó : 
// cạnh a : txt-canh-a
// cạnh b : txt-canh-b
// cạnh c : txt-canh-c

// output : nhận kết quả








document.getElementById("btn-du-doan").addEventListener( "click", function(){
    console.log("yes");
    var canhAValue = document.getElementById("txt-canh-a"). value * 1;
    console.log("canhAValue",canhAValue);
    var canhBValue = document.getElementById("txt-canh-b"). value * 1;
    console.log("canhBValue",canhBValue);
    var canhCValue = document.getElementById("txt-canh-c"). value * 1;
    console.log("canhCValue",canhCValue);
    if (canhAValue == canhBValue == canhCValue) {
        document.getElementById("txt-du-doan").value = `hình tam giác đều`
    } else if ( canhAValue == canhBValue || canhBValue == canhCValue ) {
        document.getElementById("txt-du-doan").value = `hình tam giác cân`
    } else if ( canhAValue < canhCValue && canhBValue < canhCValue && canhCValue * canhCValue  == canhAValue * canhAValue + canhBValue * canhBValue ) {
        document.getElementById("txt-du-doan").value = `hình tam giác vuông`
    }  else  {
       document.getElementById("txt-du-doan").value = `hình tam giác khác`
    }
});